# Lithuanian translations for Krita Manual package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-14 03:27+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: lt\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: ../../<rst_epilog>:38
msgid ""
".. image:: images/icons/dyna_tool.svg\n"
"   :alt: tooldyna"
msgstr ""

#: ../../reference_manual/tools/dyna.rst:1
msgid "Krita's dynamic brush tool reference."
msgstr ""

#: ../../reference_manual/tools/dyna.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/dyna.rst:11
msgid "Dyna"
msgstr ""

#: ../../reference_manual/tools/dyna.rst:16
msgid "Dynamic Brush Tool"
msgstr ""

#: ../../reference_manual/tools/dyna.rst:18
msgid "|tooldyna|"
msgstr ""

#: ../../reference_manual/tools/dyna.rst:20
msgid ""
"Add custom smoothing dynamics to your brush. This will give you similar "
"smoothing results as the normal freehand brush. There are a couple options "
"that you can change."
msgstr ""

#: ../../reference_manual/tools/dyna.rst:22
msgid "Mass"
msgstr ""

#: ../../reference_manual/tools/dyna.rst:23
msgid ""
"Average your movement to make it appear smoother. Higher values will make "
"your brush move slower."
msgstr ""

#: ../../reference_manual/tools/dyna.rst:25
msgid "Drag"
msgstr ""

#: ../../reference_manual/tools/dyna.rst:25
msgid ""
"A rubberband effect that will help your lines come back to your cursor. "
"Lower values will make the effect more extreme."
msgstr ""

#: ../../reference_manual/tools/dyna.rst:27
msgid "Recommended values are around 0.02 Mass and 0.92 Drag."
msgstr ""
