msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/docs_krita_org_tutorials___saving-"
"for-the-web.pot\n"

#: ../../tutorials/saving-for-the-web.rst:1
msgid "Tutorial for saving images for the web"
msgstr "为互联网用途保存图像的教程"

#: ../../tutorials/saving-for-the-web.rst:13
msgid "Saving For The Web"
msgstr "为互联网保存图像"

#: ../../tutorials/saving-for-the-web.rst:15
msgid ""
"Krita's default saving format is the :ref:`file_kra` format. This format "
"saves everything Krita can manipulate about an image: Layers, Filters, "
"Assistants, Masks, Color spaces, etc. However, that's a lot of data, so ``*."
"kra`` files are pretty big. This doesn't make them very good for uploading "
"to the internet. Imagine how many people's data-plans hit the limit if they "
"only could look at ``*.kra`` files! So instead, we optimise our images for "
"the web."
msgstr ""
"Krita 的默认保存格式是 :ref:`file_kra` 格式。此格式可以保存 Krita 能够对图像"
"进行的一切操作：包括图层、滤镜、辅助尺、蒙版、色彩空间等。为了保存这么多的数"
"据， ``*.kra`` 文件的体积一般都很大，对于上传到互联网来说很不方便。如果人们只"
"能使用 ``*.kra`` 文件来查看图片，他们的流量肯定两下就用光了。因此我们需要为互"
"联网用途优化输出的图像。"

#: ../../tutorials/saving-for-the-web.rst:17
msgid "There are a few steps involved:"
msgstr "这涉及到下面几步："

#: ../../tutorials/saving-for-the-web.rst:19
msgid ""
"Save as a ``.kra``. This is your working file and serves as a backup if you "
"make any mistakes."
msgstr ""
"先把图像保存为一个 ``.kra`` 文件，这是你的工作文件，同时也作为应对操作失误的"
"备份。"

#: ../../tutorials/saving-for-the-web.rst:21
msgid ""
"Flatten all layers. This turns all your layers into a single one. Just go "
"to :menuselection:`Layer --> Flatten Image` or press the :kbd:`Ctrl + Shift "
"+ E` shortcut. Flattening can take a while, so if you have a big image, "
"don't be scared if Krita freezes for a few seconds. It'll become responsive "
"soon enough."
msgstr ""

#: ../../tutorials/saving-for-the-web.rst:23
msgid ""
"Convert the color space to 8bit sRGB (if it isn't yet). This is important to "
"lower the filesize, and PNG for example can't take higher than 16bit. :"
"menuselection:`Image --> Convert Image Color Space` and set the options to "
"**RGB**, **8bit** and **sRGB-elle-v2-srgbtrc.icc** respectively. If you are "
"coming from a linear space, uncheck **little CMS** optimisations"
msgstr ""
"将色彩空间转换为 8位 sRGB (如果之前不是)。这对于减小文件体积非常有用，而且 "
"PNG 是无法保存高于 16 位的图像的。可在 :menuselection:`图像 --> 转换图像色彩"
"空间` 将相应选项设为 **RGB**、**8 位** 和 **sRGB-elle-v2-srgbtrc.icc**。如果"
"你使用了线性工作空间，不要勾选 **Little CMS 优化**。"

#: ../../tutorials/saving-for-the-web.rst:25
msgid ""
"Resize! Go to :menuselection:`Image --> Scale Image To New Size` or use the :"
"kbd:`Ctrl + Alt + I` shortcut. This calls up the resize menu. A good rule of "
"thumb for resizing is that you try to get both sizes to be less than 1200 "
"pixels. (This being the size of HD formats). You can easily get there by "
"setting the **Resolution** under **Print Size** to **72** dots per inch. "
"Then press **OK** to have everything resized."
msgstr ""

#: ../../tutorials/saving-for-the-web.rst:27
msgid ""
"Save as a web-safe image format. There's three that are especially "
"recommended:"
msgstr "把图像另存为对网络安全的格式。有三种较好的选择："

#: ../../tutorials/saving-for-the-web.rst:30
msgid "JPG"
msgstr "JPG"

#: ../../tutorials/saving-for-the-web.rst:32
msgid "Use this for images with a lot of different colors, like paintings."
msgstr "如果图像有大量不同的色彩，如传统绘画等，可选用此格式。"

#: ../../tutorials/saving-for-the-web.rst:35
msgid "PNG"
msgstr "PNG"

#: ../../tutorials/saving-for-the-web.rst:37
msgid ""
"Use this for images with few colors or which are black and white, like "
"comics and pixel-art. Select :guilabel:`Save as indexed PNG, if possible` to "
"optimise even more."
msgstr ""
"如果图像色彩不是特别丰富，如漫画那样的黑白或者像素图，可选用此格式。选中 :"
"guilabel:`尝试保存为索引 PNG` 可进一步优化图像大小。"

#: ../../tutorials/saving-for-the-web.rst:40
msgid "GIF"
msgstr "GIF"

#: ../../tutorials/saving-for-the-web.rst:42
msgid ""
"Only use this for animation (will be supported this year) or images with a "
"super low color count, because they will get indexed."
msgstr ""
"仅用于动画 (今年将实现支持)，或颜色种类特别少的图像，因为这是一个索引图像格"
"式。"

#: ../../tutorials/saving-for-the-web.rst:45
msgid "Saving with Transparency"
msgstr "保留图像透明度"

#: ../../tutorials/saving-for-the-web.rst:48
msgid ".. image:: images/Save_with_transparency.png"
msgstr ""

#: ../../tutorials/saving-for-the-web.rst:49
msgid ""
"Saving with transparency is only possible with gif and png. First, make sure "
"you see the transparency checkers (this can be done by simply hiding the "
"bottom layers, changing the projection color in :menuselection:`Image --> "
"Image Background Color and Transparency`, or by using :menuselection:"
"`Filters --> Colors --> Color to Alpha`). Then, save as PNG and tick **Store "
"alpha channel (transparency)**"
msgstr ""
"只有在使用 GIF 和 PNG 格式时才能保留图像的透明度。首先要确保你在 Krita 的画布"
"上能看见代表透明像素的棋盘格 (要做到这一点，可以隐藏最底下的图层，或者 :"
"menuselection:`图像 --> 图像背景色与透明度` ，或者 :menuselection:`滤镜 --> "
"颜色 --> 颜色转为透明度`)。最后，保存为 PNG ，勾选 **存储透明度通道**。"

#: ../../tutorials/saving-for-the-web.rst:51
msgid "Save your image, upload, and show it off!"
msgstr "点击保存，然后就可以把输出的图像上传并分享了！"
