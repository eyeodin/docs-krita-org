# translation of docs_krita_org_reference_manual___main_menu___window_menu.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___main_menu___window_menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-04-02 12:40+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<generated>:1
msgid "List of open documents."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:1
msgid "The window menu in Krita."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:11
#, fuzzy
#| msgid "New Window"
msgid "Window"
msgstr "Nové okno"

#: ../../reference_manual/main_menu/window_menu.rst:11
#, fuzzy
#| msgid "New View"
msgid "View"
msgstr "Nový pohľad"

#: ../../reference_manual/main_menu/window_menu.rst:16
msgid "Window Menu"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:18
msgid "A menu completely dedicated to window management in Krita."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:20
msgid "New Window"
msgstr "Nové okno"

#: ../../reference_manual/main_menu/window_menu.rst:21
msgid "Creates a new window for Krita. Useful with multiple screens."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:22
msgid "New View"
msgstr "Nový pohľad"

#: ../../reference_manual/main_menu/window_menu.rst:23
msgid ""
"Make a new view of the given document. You can have different zoom or "
"rotation on these."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:24
msgid "Workspace"
msgstr "Pracovná plocha"

#: ../../reference_manual/main_menu/window_menu.rst:25
msgid "A convenient access panel to the :ref:`resource_workspaces`."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:26
msgid "Close"
msgstr "Zavrieť"

#: ../../reference_manual/main_menu/window_menu.rst:27
msgid "Close the current view."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:28
msgid "Close All"
msgstr "Zavrieť všetko"

#: ../../reference_manual/main_menu/window_menu.rst:29
#, fuzzy
#| msgid "Close all documents"
msgid "Close all documents."
msgstr "Zatvoriť všetky dokumenty"

#: ../../reference_manual/main_menu/window_menu.rst:30
msgid "Tile"
msgstr "Dlaždice"

#: ../../reference_manual/main_menu/window_menu.rst:31
msgid "Tiles all open documents into a little sub-window."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:32
msgid "Cascade"
msgstr "Kaskáda"

#: ../../reference_manual/main_menu/window_menu.rst:33
msgid "Cascades the sub-windows."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:34
msgid "Next"
msgstr "Nasledujúci"

#: ../../reference_manual/main_menu/window_menu.rst:35
msgid "Selects the next view."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:36
msgid "Previous"
msgstr "Predchádzajúci"

#: ../../reference_manual/main_menu/window_menu.rst:37
msgid "Selects the previous view."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:39
msgid "Use this to switch between documents."
msgstr ""
