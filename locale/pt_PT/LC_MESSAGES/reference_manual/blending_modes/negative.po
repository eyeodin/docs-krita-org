# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-18 10:07+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Right BlendingmodesArcusTangentSampleimagewithdots\n"
"X-POFile-SpellExtra: Krita image bmdifference\n"
"X-POFile-SpellExtra: BlendingmodesDifferenceSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesEquivalenceSampleimagewithdots\n"
"X-POFile-SpellExtra: blendingmodes images\n"
"X-POFile-SpellExtra: BlendingmodesExclusionSampleimagewithdots ref\n"
"X-POFile-SpellExtra: BlendingmodesAdditiveSubtractiveSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesNegationSampleimagewithdots abs\n"

#: ../../reference_manual/blending_modes/negative.rst:1
msgid ""
"Page about the negative blending modes in Krita: Additive Subtractive, Arcus "
"Tangent, Difference, Equivalence, Exclusion, Negation."
msgstr ""
"Página sobre os modos de mistura negativa no Krita: Aditivo/Subtractivo, "
"Arco-Tangente, Diferença, Equivalência, Exclusão, Negação."

#: ../../reference_manual/blending_modes/negative.rst:12
#: ../../reference_manual/blending_modes/negative.rst:16
msgid "Negative"
msgstr "Negativo"

#: ../../reference_manual/blending_modes/negative.rst:18
msgid "These are all blending modes which seem to make the image go negative."
msgstr ""
"Estes são todos os modos de mistura que fazem com que a imagem pareça um "
"negativo."

#: ../../reference_manual/blending_modes/negative.rst:20
#: ../../reference_manual/blending_modes/negative.rst:24
msgid "Additive Subtractive"
msgstr "Aditivo-Subtractivo"

#: ../../reference_manual/blending_modes/negative.rst:25
msgid "Subtract the square root of the lower layer from the upper layer."
msgstr "Subtrai a raiz quadrada da camada inferior da camada superior."

#: ../../reference_manual/blending_modes/negative.rst:30
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Additive_Subtractive_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Additive_Subtractive_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:30
msgid "Left: **Normal**. Right: **Additive Subtractive**."
msgstr "Esquerda: **Normal**. Direita: **Aditivo/Subtractivo**."

#: ../../reference_manual/blending_modes/negative.rst:32
#: ../../reference_manual/blending_modes/negative.rst:36
msgid "Arcus Tangent"
msgstr "Arco-Tangente"

#: ../../reference_manual/blending_modes/negative.rst:38
msgid ""
"Divides the lower layer by the top. Then divides this by Pi. Then uses that "
"in an Arc tangent function, and multiplies it by two."
msgstr ""
"Divide a camada inferior pela do topo. Depois divide o valor por Pi. "
"Finalmente, usa o resultado numa função de arco-tangente e multiplica-a por "
"dois."

#: ../../reference_manual/blending_modes/negative.rst:44
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Arcus_Tangent_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Arcus_Tangent_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:44
msgid "Left: **Normal**. Right: **Arcus Tangent**."
msgstr "Esquerda: **Normal**. Direita: **Arco-Tangente**."

#: ../../reference_manual/blending_modes/negative.rst:46
#: ../../reference_manual/blending_modes/negative.rst:50
msgid "Difference"
msgstr "Diferença"

#: ../../reference_manual/blending_modes/negative.rst:52
msgid ""
"Checks per pixel of which layer the pixel-value is highest/lowest, and then "
"subtracts the lower value from the higher-value."
msgstr ""
"Verifica, pixel a pixel, qual a camada cujo valor do pixel é maior/menor, "
"subtraindo depois o valor menor do maior."

#: ../../reference_manual/blending_modes/negative.rst:58
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Difference_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Difference_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:58
msgid "Left: **Normal**. Right: **Difference**."
msgstr "Esquerda: **Normal**. Direita: **Diferença**."

#: ../../reference_manual/blending_modes/negative.rst:60
#: ../../reference_manual/blending_modes/negative.rst:64
msgid "Equivalence"
msgstr "Equivalência"

#: ../../reference_manual/blending_modes/negative.rst:66
msgid ""
"Subtracts the underlying layer from the upper-layer. Then inverts that. "
"Seems to produce the same result as :ref:`bm_difference`."
msgstr ""
"Subtrai a camada subjacente da camada superior. Depois inverte isso. Parece "
"produzir o mesmo resultado que a :ref:`bm_difference`."

#: ../../reference_manual/blending_modes/negative.rst:72
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Equivalence_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Equivalence_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:72
msgid "Left: **Normal**. Right: **Equivalence**."
msgstr "Esquerda: **Normal**. Direita: **Equivalência**."

#: ../../reference_manual/blending_modes/negative.rst:74
#: ../../reference_manual/blending_modes/negative.rst:78
msgid "Exclusion"
msgstr "Exclusão"

#: ../../reference_manual/blending_modes/negative.rst:80
msgid ""
"This multiplies the two layers, adds the source, and then subtracts the "
"multiple of two layers twice."
msgstr ""
"Isto multiplica as duas camadas, adiciona a origem e depois subtrai duas "
"vezes o múltiplo das duas camadas."

#: ../../reference_manual/blending_modes/negative.rst:85
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Exclusion_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Exclusion_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:85
msgid "Left: **Normal**. Right: **Exclusion**."
msgstr "Esquerda: **Normal**. Direita: **Exclusão**."

#: ../../reference_manual/blending_modes/negative.rst:87
#: ../../reference_manual/blending_modes/negative.rst:91
msgid "Negation"
msgstr "Negativo"

#: ../../reference_manual/blending_modes/negative.rst:93
msgid ""
"The absolute of the 1.0f value subtracted by base subtracted by the blend "
"layer. abs(1.0f - Base - Blend)"
msgstr ""
"O valor absoluto de 1.0f subtraído à base e à camada de mistura. abs(1.0f - "
"Base - Mistura)"

#: ../../reference_manual/blending_modes/negative.rst:98
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Negation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Negation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:98
msgid "Left: **Normal**. Right: **Negation**."
msgstr "Esquerda: **Normal**. Direita: **Negação**."
