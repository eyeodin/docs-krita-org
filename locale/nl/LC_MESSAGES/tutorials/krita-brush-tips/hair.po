# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-07 10:38+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../tutorials/krita-brush-tips/hair.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-hair_01.png\n"
"   :alt: some examples of hair brush"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-hair_01.png\n"
"   :alt: enige voorbeelden van haarpenseel"

#: ../../tutorials/krita-brush-tips/hair.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-hair_02.png\n"
"   :alt: curve setting in brush editor"
msgstr ""

#: ../../tutorials/krita-brush-tips/hair.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-hair_03.png\n"
"   :alt: brush-tip dialog"
msgstr ""

#: ../../tutorials/krita-brush-tips/hair.rst:1
msgid "A tutorial about painting hair in Krita"
msgstr "Een inleiding over tekenen van haar in Krita"

#: ../../tutorials/krita-brush-tips/hair.rst:13
msgid "Brush-tips:Hair"
msgstr ""

#: ../../tutorials/krita-brush-tips/hair.rst:18
msgid ""
"Usually, most digital styles tend to focus on simple brushes, like the round "
"brushes, and their usage in hair is no different. So, the typical example "
"would be the one on the left, where we use *fill_round* to draw a silhouette "
"and build up to lighter values."
msgstr ""

#: ../../tutorials/krita-brush-tips/hair.rst:20
msgid ""
"The reason I use *fill_round* here is because the pressure curve on the size "
"is s-shaped. My tablet has a spring-loaded nib which also causes and s-"
"shaped curve hard-ware wise. This means that it becomes really easy to draw "
"thin lines and big lines. Having a trained inking hand helps a lot with this "
"as well, and it’s something you build up over time."
msgstr ""

#: ../../tutorials/krita-brush-tips/hair.rst:25
msgid ""
"We then gloss the shadow parties with the *basic_tip_default*. So you can "
"get really far with basic brushes and basic painting skills and indeed I am "
"almost convinced tysontan, who draws our mascot, doesn’t use anything but "
"the *basic_tip_default* sometimes."
msgstr ""

#: ../../tutorials/krita-brush-tips/hair.rst:30
msgid ""
"However, if you want an easy hair brush, just take the *fill_round*, go to "
"the brush-tip, pick predefined and select *A2-sparkle-1* as the brush tip. "
"You can fiddle with the spacing below the selection of predefined brushtip "
"to space the brush, but I believe the default should be fine enough to get "
"result."
msgstr ""
