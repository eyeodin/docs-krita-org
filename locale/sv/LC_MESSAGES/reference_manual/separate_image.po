# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:26+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Output to color, not grayscale."
msgstr "Utdata i färg, inte gråskala."

#: ../../reference_manual/separate_image.rst:1
msgid "The channel separation dialog in Krita"
msgstr "Dialogrutan för kanalseparation i Krita"

#: ../../reference_manual/separate_image.rst:10
#: ../../reference_manual/separate_image.rst:15
msgid "Separate Image"
msgstr "Separera bild"

#: ../../reference_manual/separate_image.rst:10
msgid "Channel Separation"
msgstr "Kanalseparation"

#: ../../reference_manual/separate_image.rst:17
msgid ""
"The separate image dialog allows you to separate the channels of the image "
"into layers. This is useful in game texturing workflows, as well as more "
"advanced CMYK workflows. When confirming, :guilabel:`Separate Image` creates "
"a series of layers for each channel. So for RGB, it will create Red, Green "
"and Blue layers, each representing the pixels of that channel, and for CMYK, "
"Cyan, Magenta, Yellow and Key layers."
msgstr ""
"Den separata bilddialogrutan låter dig separera kanalerna i en bild till "
"lager. Det är användbart i arbetsflöden för spelstrukturer, samt mer "
"avancerade CMYK-arbetsflöden. Vid bekräftelse skapar :guilabel:`Separera "
"bild` en serie lager för varje kanal. För RGB skapar det lagren Röd, Grön "
"och Blå, där vart och ett representerar den kanalens bildpunkter, och för "
"CMYK lagren Turkos, Magenta,Gul och Svart."

#: ../../reference_manual/separate_image.rst:20
msgid "Which part of the image should be separated."
msgstr "Vilken del av bilden som ska separeras."

#: ../../reference_manual/separate_image.rst:22
msgid "Current Layer"
msgstr "Aktuellt lager"

#: ../../reference_manual/separate_image.rst:23
msgid "The active layer."
msgstr "Det aktiva lagret."

#: ../../reference_manual/separate_image.rst:25
msgid "Source"
msgstr "Källa"

#: ../../reference_manual/separate_image.rst:25
msgid "Flatten all layers before separation"
msgstr "Platta ut alla lager innan separering"

#: ../../reference_manual/separate_image.rst:25
msgid "The total image."
msgstr "Hela bilden."

#: ../../reference_manual/separate_image.rst:28
msgid ""
"What to do with the alpha channel. All Krita images have an alpha channel, "
"which is the transparency. How should it be handled when separating?"
msgstr ""
"Vad man ska göra med alfakanalen. Alla Krita-bilder har en alfakanal, som är "
"genomskinligheten. Hur ska den hanteras när man separerar?"

#: ../../reference_manual/separate_image.rst:30
msgid "Copy Alpha Channel to Each separate channel as an alpha channel."
msgstr "Kopiera alfakanal till varje separerad kanal som en alfakanal."

#: ../../reference_manual/separate_image.rst:31
msgid ""
"This adds the transparency of the original source to all the separation "
"layers."
msgstr ""
"Det lägger till genomskinligheten i originalkällan till alla "
"separationslager."

#: ../../reference_manual/separate_image.rst:32
msgid "Discard Alpha Channel"
msgstr "Kasta alfakanal"

#: ../../reference_manual/separate_image.rst:33
msgid "Just get rid of the alpha altogether."
msgstr "Bli bara av med alfa helt och hållet."

#: ../../reference_manual/separate_image.rst:35
msgid "Alpha Options"
msgstr "Alfaalternativ"

#: ../../reference_manual/separate_image.rst:35
msgid "Create separate separation from alpha channel"
msgstr "Skapa egen separation från alfakanalen"

#: ../../reference_manual/separate_image.rst:35
msgid "Create a separate layer with the alpha channel as a grayscale layer."
msgstr "Skapa ett separat lager med alfakanalen som ett gråskalelager."

#: ../../reference_manual/separate_image.rst:37
msgid "Downscale to 8bit before separating."
msgstr "..Skala ner till 8 bitar innan separering."

#: ../../reference_manual/separate_image.rst:38
msgid "Convert the image to 8bit before doing the separation."
msgstr "Konvertera bilden till 8 bitar innan separationen utförs."

#: ../../reference_manual/separate_image.rst:40
msgid ""
"This results in the Red separation using black to red instead of black to "
"white. This can make it easier to recombine using certain methods."
msgstr ""
"Det resulterar i att separation av röd använder svart till röd istället för "
"svart till vit. Det kan göra det enklare att återkombinera med användning av "
"vissa metoder."
