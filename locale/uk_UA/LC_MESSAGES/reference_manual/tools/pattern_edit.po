# Translation of docs_krita_org_reference_manual___tools___pattern_edit.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___pattern_edit\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:26+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:20
msgid ""
".. image:: images/icons/pattern_tool.svg\n"
"   :alt: toolpatternedit"
msgstr ""
".. image:: images/icons/pattern_tool.svg\n"
"   :alt: toolpatternedit"

#: ../../reference_manual/tools/pattern_edit.rst:1
msgid "Krita's vector pattern editing tool reference."
msgstr "Довідник із інструмента редагування векторних візерунків Krita."

#: ../../reference_manual/tools/pattern_edit.rst:11
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/tools/pattern_edit.rst:11
msgid "Pattern"
msgstr "Візерунок"

#: ../../reference_manual/tools/pattern_edit.rst:16
msgid "Pattern Editing Tool"
msgstr "Інструмент «Редагування візерунків»"

#: ../../reference_manual/tools/pattern_edit.rst:18
msgid "|toolpatternedit|"
msgstr "|toolpatternedit|"

#: ../../reference_manual/tools/pattern_edit.rst:22
msgid ""
"The pattern editing tool has been removed in 4.0, currently there's no way "
"to edit pattern fills for vectors."
msgstr ""
"Інструмент редагування візерунків було вилучено у версії 4.0. У поточній "
"версії не передбачено засобів редагування заповнення візерунком векторних "
"форм."

#: ../../reference_manual/tools/pattern_edit.rst:24
msgid ""
"The Pattern editing tool works on Vector Shapes that use a Pattern fill. On "
"these shapes, the Pattern Editing Tool allows you to change the size, ratio "
"and origin of a pattern."
msgstr ""
"Інструмент редагування візерунків працює із векторними формами, які "
"заповнено візерунком. На цих формах інструмент редагування візерунків "
"уможливлює зміну розміру, пропорцій та походження візерунка."

#: ../../reference_manual/tools/pattern_edit.rst:27
msgid "On Canvas-editing"
msgstr "Редагування на полотні"

#: ../../reference_manual/tools/pattern_edit.rst:29
msgid ""
"You can change the origin by click dragging the upper node, this is only "
"possible in Tiled mode."
msgstr ""
"Щоб змінити походження візерунка наведіть вказівник миші на верхній вузол, "
"натисніть і утримуйте ліву кнопку миші і перетягніть вузол. Це можливо лише "
"у режимі «Плиткою»."

#: ../../reference_manual/tools/pattern_edit.rst:31
msgid ""
"You can change the size and ratio by click-dragging the lower node. There's "
"no way to constrain the ratio in on-canvas editing, this is only possible in "
"Original and Tiled mode."
msgstr ""
"Щоб змінити розмір і пропорції, затисніть і перетягніть нижній вузол. Під "
"час редагування безпосередньо на полотні не передбачено можливості обмеження "
"пропорцій (фіксування співвідношення висота-ширина). Така зміна пропорцій "
"можлива лише у режимі «Оригінал» і «Плиткою»."

#: ../../reference_manual/tools/pattern_edit.rst:34
msgid "Tool Options"
msgstr "Параметри інструмента"

#: ../../reference_manual/tools/pattern_edit.rst:36
msgid "There are several tool options with this tool, for fine-tuning:"
msgstr ""
"Для коригування поведінки цього інструмента передбачено декілька параметрів:"

#: ../../reference_manual/tools/pattern_edit.rst:38
msgid "First there are the Pattern options."
msgstr "По-перше, передбачено параметри візерунка."

#: ../../reference_manual/tools/pattern_edit.rst:41
msgid "This can be set to:"
msgstr "Тут можна встановити такі варіанти:"

#: ../../reference_manual/tools/pattern_edit.rst:43
msgid "Original:"
msgstr "Початковий:"

#: ../../reference_manual/tools/pattern_edit.rst:44
msgid "This will only show one, unstretched, copy of the pattern."
msgstr "Тут буде показано одну, нерозтягнену, копію візерунка."

#: ../../reference_manual/tools/pattern_edit.rst:45
msgid "Tiled (Default):"
msgstr "Мозаїка (типовий):"

#: ../../reference_manual/tools/pattern_edit.rst:46
msgid "This will let the pattern appear tiled in the x and y direction."
msgstr ""
"За допомогою цього режиму можна створити мозаїку з плиток із візерунком у "
"напрямках осей x та y."

#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "Repeat:"
msgstr "Повторення:"

#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "Stretch:"
msgstr "Розтягнути:"

#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "This will stretch the Pattern image to the shape."
msgstr ""
"За допомогою цього режиму можна розтягнути зображення візерунка до розмірів "
"форми."

#: ../../reference_manual/tools/pattern_edit.rst:51
msgid "Pattern origin. This can be set to:"
msgstr "Оригінал візерунка. Можливі варіанти:"

#: ../../reference_manual/tools/pattern_edit.rst:53
msgid "Top-left"
msgstr "Вгорі ліворуч"

#: ../../reference_manual/tools/pattern_edit.rst:54
msgid "Top"
msgstr "Вгорі"

#: ../../reference_manual/tools/pattern_edit.rst:55
msgid "Top-right"
msgstr "Вгорі праворуч"

#: ../../reference_manual/tools/pattern_edit.rst:56
msgid "Left"
msgstr "Ліворуч"

#: ../../reference_manual/tools/pattern_edit.rst:57
msgid "Center"
msgstr "За центром"

#: ../../reference_manual/tools/pattern_edit.rst:58
msgid "Right"
msgstr "Праворуч"

#: ../../reference_manual/tools/pattern_edit.rst:59
msgid "Bottom-left"
msgstr "Внизу ліворуч"

#: ../../reference_manual/tools/pattern_edit.rst:60
msgid "Bottom"
msgstr "Внизу"

#: ../../reference_manual/tools/pattern_edit.rst:61
msgid "Reference point:"
msgstr "Точка відліку:"

#: ../../reference_manual/tools/pattern_edit.rst:61
msgid "Bottom-right."
msgstr "Внизу праворуч."

#: ../../reference_manual/tools/pattern_edit.rst:64
msgid "For extra tweaking, set in percentages."
msgstr "Для точнішого налаштовування можна встановити у відсотках."

#: ../../reference_manual/tools/pattern_edit.rst:66
msgid "X:"
msgstr "X:"

#: ../../reference_manual/tools/pattern_edit.rst:67
msgid "Offset in the X coordinate, so horizontally."
msgstr "Відступ за координатою X, отже, горизонтально."

#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Reference Point Offset:"
msgstr "Відступ точки відліку:"

#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Y:"
msgstr "Y:"

#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Offset in the Y coordinate, so vertically."
msgstr "Відступ за координатою Y, отже, вертикально."

#: ../../reference_manual/tools/pattern_edit.rst:71
msgid "Tile Offset:"
msgstr "Відступ плитки:"

#: ../../reference_manual/tools/pattern_edit.rst:72
msgid "The tile offset if the pattern is tiled."
msgstr "Відступ плитки, якщо візерунок є мозаїчним."

#: ../../reference_manual/tools/pattern_edit.rst:74
msgid "Fine Tune the resizing of the pattern."
msgstr "Коригування зміни розмірів візерунка."

#: ../../reference_manual/tools/pattern_edit.rst:76
msgid "W:"
msgstr "Ш:"

#: ../../reference_manual/tools/pattern_edit.rst:77
msgid "The width, in pixels."
msgstr "Ширина у пікселях."

#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "Pattern Size:"
msgstr "Розмір візерунка:"

#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "H:"
msgstr "В:"

#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "The height, in pixels."
msgstr "Висота у пікселях."

#: ../../reference_manual/tools/pattern_edit.rst:81
msgid ""
"And then there's :guilabel:`Patterns`, which is a mini pattern docker, and "
"where you can pick the pattern used for the fill."
msgstr ""
"Крім того, передбачено :guilabel:`Візерунки` — невеличку бічну панель, за "
"допомогою якої ви можете вибрати візерунок для заповнення."
